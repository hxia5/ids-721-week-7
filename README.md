# IDS 721 Week 7
[![pipeline status](https://gitlab.com/hxia5/ids-721-week-7/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-7/-/commits/main)

## Overview
* This is my repository ofIDS 721 Mini-Project 7 - Data Processing with Vector Database.

## Purpose
- Ingest data into Vector database
- Perform queries and aggregations
- Visualize output

## Qdrant Collection 

![alt text](test.png)

## Sample Viz 
My query is "flower": `http://localhost:9000/no/yes?q=%27flower%27`

![alt text](q.png)

## Key Steps

1. Install Rust and Cargo-Lambda, instructions can be found [here](https://www.cargo-lambda.info/guide/installation.html) and [here](https://www.rust-lang.org/tools/install).

2. Create a new Rust project using the following command:
```bash
cargo new ids-721-week-7
```

3. Sign up for qdrant, click on `Cloud`, follow the instructions to create a new collection, and get the API key and url, store them in `.env` file.

4. Sign up for cohere and get api key, store it in `.env` file.

5. Edits the `main.rs` file and `setup.rs` file, read the `plants.jsonl` to pass into data ingestion and query the data.

6. Run bin to do data ingenstion using `cargo run --bin setup_collection plants.jsonl`

7. Go to the qdrant cluster, there will be a collection called `test`.

8. Do visualizations and aggregations in main.rs, which will give two data that related to the query.

9. Do `cargo lambda watch` to test locally.

10. Test you viz and aggreation works by sending a GET request: `http://localhost:9000/no/yes?q=%27flower%27`



## References
1. https://github.com/qdrant/rust-client
2. https://github.com/qdrant/internal-examples/blob/master/lambda-search/src/setup_collection.rs
3. https://docs.cohere.com/docs/the-cohere-platform